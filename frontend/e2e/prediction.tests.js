import Page from './page-objects';

const PageObjects = new Page();

fixture `Happy Path`
    .page(`http://localhost:3000`)

test('Should predict the time', async t => {
    await t
        .expect(PageObjects.originInput.value).eql("")
        .expect(PageObjects.destinationInput.value).eql("")
        .expect(PageObjects.transferTime.value).eql("")
        .expect(PageObjects.submit.hasAttribute('disabled')).ok()

        .click(PageObjects.origin)
        .click(PageObjects.orignOptions.nth(0))
        .click(PageObjects.destination)
        .click(PageObjects.destinationOptions.nth(1))
        .typeText(PageObjects.transferTime, "15:00")

        .expect(PageObjects.originInput.value).notEql("")
        .expect(PageObjects.destinationInput.value).notEql("")

        .expect(PageObjects.submit.hasAttribute('disabled')).notOk()
        .click(PageObjects.submit)
        .wait(1000)
        
        .expect(PageObjects.submit.hasAttribute('hidden')).notOk()
        .expect(PageObjects.result.innerText).notEql("")
        .expect(PageObjects.result.innerText).match(/^\d{2}:\d{2}\s(same|next)\sday/);

});
