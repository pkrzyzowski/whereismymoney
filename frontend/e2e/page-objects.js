import { Selector } from 'testcafe';

export default class Page {
    constructor () {
        this.origin = Selector('#origin');
        this.orignOptions = Selector("#menu-origin").find('li');
        this.originInput = this.origin.find("input")

        this.destination = Selector('#destination');
        this.destinationOptions = Selector("#menu-destination").find('li')
        this.destinationInput = this.destination.find("input")

        this.transferTime = Selector('#transferTime');
        this.submit = Selector("#submit");

        this.result = Selector(".prediction b");
    }
}
