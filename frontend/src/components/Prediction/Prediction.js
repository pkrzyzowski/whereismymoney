import React, { Component } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import { FormControl } from '@material-ui/core';
import axios from 'axios';

import './Prediction.css';

class Prediction extends Component {

  state = {
    origin: "",
    destination: "",
    transferTime: "",
    predictedTime: null,
    banks: [],
    networkError: false
  }

  componentDidMount() {
    axios.get('/api/bank')
      .then(response => {
        this.setState({ banks: response.data });
      }).catch(error => {
        this.setState({ networkError: true });
      });
  }

  renderBanks() {
    if(!this.state.banks) {
      return "";
    }

    return this.state.banks.map(x => (<MenuItem key={x} value={x}>{x}</MenuItem>));
  }

  updateStateValue = event => { 
    this.setState({ [event.target.name]: event.target.value });
  }

  predict = () => {
    axios.get(`/api/prediction/${this.state.origin}/${this.state.destination}/${this.state.transferTime}`)
      .then(response => {
        this.setState({ predictedTime: response.data });
      }).catch(error => {
        this.setState({ networkError: true });
      })
  }

  render() {
    const valid = this.state.origin !== ""
      && this.state.destination !== ""
      && this.state.transferTime !== "";

    const predictionHidden = this.state.predictedTime === null;
    let predictedTime = "";
    let predictedDay = "";

    if (this.state.predictedTime) {
      // In case of more complex formats, I'd probably use moment. However it should enough to just slice it here.
      predictedTime = this.state.predictedTime.time.slice(0, -3);
      predictedDay = this.state.predictedTime.nextDay ? " next day" : " same day";
    }

    return (
      <div className="prediction-component">
        <div className="error" hidden={!this.state.networkError}>Couldn't communicate with server - please refresh the page and if the problem persists, contact administrator.</div>
        <div className="instruction">
          Please select the transfers origin bank, and its destination then click "So where is it?" to see when you should get the money. 
        </div>
        <div className="options">
          <FormControl id="origin">
            <InputLabel>Transfer origin</InputLabel>
            <Select value={this.state.origin} className="select" onChange={this.updateStateValue} inputProps={{ name: 'origin' }}>
              {this.renderBanks()}
            </Select>
          </FormControl>
          &nbsp;-&nbsp; 
          <FormControl id="destination">
            <InputLabel>Your bank</InputLabel>
            <Select value={this.state.destination} className="select" onChange={this.updateStateValue} inputProps={{ name: 'destination' }}>
              {this.renderBanks()}
            </Select>
          </FormControl>
          &nbsp;-&nbsp;
          <TextField id="transferTime" className="timePicker" name="transferTime" label="Transfer time:" type="time" defaultValue=""
            InputLabelProps={{
              shrink: true, 
            }}
            inputProps={{ step: 300 }}
            onChange={this.updateStateValue}
          />
        </div>
        <div className="actions">
          <Button id="submit" variant="contained" color="primary" disabled={!valid} onClick={this.predict}>So where is it?</Button>
        </div>
        <div className="prediction" hidden={predictionHidden}>
            The money should be available in your account around <b>{predictedTime} {predictedDay}</b>.
        </div>
      </div>
    );
  }
}

export default Prediction;
