import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Prediction from '../Prediction/Prediction';
import BankEditor from '../BankEditor/BankEditor';
import BanksList from '../BanksList/BanksList';

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <div className="header">
          Where is my money?
        </div>
        <Router>
          <div className="container">
            <Route exact path="/" component={Prediction} />
            <Route exact path="/configuration" component={BanksList} />
            <Route exact path="/configuration/bank/:id" component={BankEditor} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
