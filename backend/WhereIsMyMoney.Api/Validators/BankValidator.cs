﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhereIsMyMoney.Api.Dto;

namespace WhereIsMyMoney.Api.Validators
{
    public class BankValidator : AbstractValidator<Bank>
    {
        public BankValidator()
        {
            RuleFor(x => x.Name)
                .NotNull().WithMessage("Bank name cannot be empty.")
                .NotEmpty().WithMessage("Bank name cannot be empty.")
                .Matches(@"^[\p{L}\d\s\.\-]*$").WithMessage("Bank name contains characters that are not allowed.")
                .Length(3, 100).WithMessage("Bank name cannot be smaller than 3 characters and cannot be higher than 100 characters.");

            var elixirSessionValidator = new ElixirSessionValidator();
            RuleForEach(x => x.ElixirSessions).SetValidator(elixirSessionValidator);
        }
    }
}
