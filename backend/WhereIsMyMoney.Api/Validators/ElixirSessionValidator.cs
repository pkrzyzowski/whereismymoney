﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WhereIsMyMoney.Api.Dto;

namespace WhereIsMyMoney.Api.Validators
{
    public class ElixirSessionValidator : AbstractValidator<ElixirSession>
    {
        public ElixirSessionValidator()
        {
            RuleFor(x => x.SessionTime)
                .NotNull().WithMessage("Session time cannot be empty.")
                .Matches(@"\d\d:\d\d").WithMessage("Please provide a time in the following format: \"HH:mm\"");

            RuleFor(x => x.Type).NotNull().WithMessage("Session type is required.");
        }
    }
}
