﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System.IO;
using System.Threading.Tasks;
using WhereIsMyMoney.Api.Model;
using WhereIsMyMoney.Api.Repository;
using WhereIsMyMoney.Api.Settings;

namespace WhereIsMyMoney.Api
{
    // NOTE: I'm not securing the endpoints in this demo application,
    // however in the production environment I'd use IdentityServer4 and integrate RBAC in this API.:)
    // Especially that it's going to be included as a part of .net core: https://twitter.com/SYeganegi/status/1042236135374610432
    public class Startup
    {
        private readonly IConfiguration config;

        public Startup(IHostingEnvironment env)
        {
            var configBuilder = new ConfigurationBuilder();

            configBuilder
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("config.json", optional: true, reloadOnChange: true);

            if (env.IsDevelopment())
            {
                configBuilder.AddUserSecrets<Startup>();
            }

            this.config = configBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            DatabaseSettings databaseSettings = new DatabaseSettings();
            config.Bind("DatabaseSettings", databaseSettings);
            services.SetupMongoDb(databaseSettings);
            services.AddScoped<IRepository<Bank>, BankRepository>();
            services.AddLogging();
            services.ConfigureMapper();
            services.AddMvcCore().AddJsonFormatters();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
