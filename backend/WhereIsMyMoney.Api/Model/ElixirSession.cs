﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Model
{
    public class ElixirSession
    {
        public ElixirSessionType Type { get; set; }
        public TimeSpan SessionTime { get; set; }
    }
}
