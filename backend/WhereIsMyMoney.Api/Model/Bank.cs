﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Model
{
    public class Bank
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public IList<ElixirSession> ElixirSessions { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
    }
}
