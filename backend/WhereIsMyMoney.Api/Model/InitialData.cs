﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Model
{
    public class InitialData
    {
        public static List<Bank> Banks = new List<Bank>
        {
            new Bank
            {
                Name = "Alior Bank",
                ElixirSessions = new List<ElixirSession>
                {
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(11,0,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(15,0,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(17,0,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(9,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(13,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(16,0,0)
                    }
                },
                Created = DateTime.Now,
            },
            new Bank
            {
                Name = "Bank BPH",
                ElixirSessions = new List<ElixirSession>
                {
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(11,45,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(15,45,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(17,0,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(9,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(14,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(17,0,0)
                    }
                },
                Created = DateTime.Now,
            },
            new Bank
            {
                Name = "ING Bank Śląski",
                ElixirSessions = new List<ElixirSession>
                {
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(11,0,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(15,0,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(17,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(8,10,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(11,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(14,30,0)
                    }
                },
                Created = DateTime.Now,
            },
            new Bank
            {
                Name = "City Handlowy",
                ElixirSessions = new List<ElixirSession>
                {
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(10,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(14,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Incoming,
                        SessionTime = new TimeSpan(17,30,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(8,00,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(12,15,0)
                    },
                    new ElixirSession
                    {
                        Type = ElixirSessionType.Outcoming,
                        SessionTime = new TimeSpan(15,30,0)
                    }
                },
                Created = DateTime.Now,
            }
        };
    }
}
