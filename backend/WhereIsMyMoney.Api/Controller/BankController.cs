﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WhereIsMyMoney.Api.Model;
using WhereIsMyMoney.Api.Repository;

namespace WhereIsMyMoney.Api.Controller
{
    [Route("api/bank")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private IRepository<Bank> repository;
        private IMapper mapper;
        private ILogger<BankController> logger;

        public BankController(IRepository<Bank> repository, IMapper mapper, ILogger<BankController> logger)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet("{name?}")]
        public async Task<IActionResult> Get(string name)
        {
            if(string.IsNullOrWhiteSpace(name))
            {
                try
                {   
                    var banks = await repository.Read();
                    return Ok(banks.Select(x => x.Name));
                }
                catch (Exception e)
                {
                    logger.LogError($"An unhandled exception occured while getting banks list from the repository. Exception message: {e.ToString()}");
                    return StatusCode(500);
                }
            }
            try
            {
                var bank = await repository.ReadOne(x => x.Name == name);
                if(bank == null)
                {
                    return NotFound();
                }

                return Ok(mapper.Map<Dto.Bank>(bank));
            }
            catch (Exception e)
            {
                logger.LogError($"An unhandled exception occured while trying to read bank {name} object from the repository. Exception message: {e.ToString()}");
                return StatusCode(500);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Dto.Bank bank)
        {
            try
            {
                if(await repository.Exists(x => x.Name == bank.Name))
                {
                    return BadRequest("Provided bank already exists.");
                }

                var createdBank = await repository.Create(mapper.Map<Model.Bank>(bank));
                string newBankUri = Request.Path.Add(new PathString($"/{createdBank.Name}")).ToString();
                return Created(newBankUri, mapper.Map<Dto.Bank>(createdBank));
            }
            catch (Exception e)
            {
                logger.LogError($"An unhandled exception occured while trying to create a bank in the repository. Exception message: {e.ToString()}");
                return StatusCode(500);
            }
        }

        [HttpPut("{name?}")]
        public async Task<IActionResult> Put(string name, [FromBody] Dto.Bank bank)
        {
            try
            {
                if(!await repository.Exists(x => x.Name == name))
                {
                    return NotFound();
                }

                var updatedBank = await repository.Update(x => x.Name == name, mapper.Map<Model.Bank>(bank));
                return Ok(updatedBank);
            }
            catch (Exception e)
            {
                logger.LogError($"An unhandled exception occured while trying to update a bank in the repository. Exception message: {e.ToString()}");
                return StatusCode(500);
            }
        }

        [HttpDelete("{name?}")]
        public async Task<IActionResult> Delete(string name)
        {
            try
            {
                if(!await repository.Exists(x => x.Name == name))
                {
                    return NotFound();
                }

                await repository.Delete(x => x.Name == name);
                return Ok();
            }
            catch (Exception e)
            {
                logger.LogError($"An unhandled exception occured while trying to delete a bank from the repository. Exception message: {e.ToString()}");
                return StatusCode(500);
            }
        }
    }
}