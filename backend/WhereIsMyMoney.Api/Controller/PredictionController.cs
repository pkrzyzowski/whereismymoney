﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WhereIsMyMoney.Api.Dto;
using WhereIsMyMoney.Api.Model;
using WhereIsMyMoney.Api.Repository;

namespace WhereIsMyMoney.Api.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class PredictionController : ControllerBase
    {
        private IRepository<Model.Bank> repository;
        ILogger<PredictionController> logger;

        public PredictionController(IRepository<Model.Bank> repository, ILogger<PredictionController> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        [HttpGet]
        [Route("{origin}/{destination}/{transferTime}")]
        public async Task<IActionResult> Get(string origin, string destination, TimeSpan transferTime)
        {
            if (string.IsNullOrWhiteSpace(origin))
            {
                return BadRequest("Source bank name is required.");
            }
            
            if (string.IsNullOrWhiteSpace(destination))
            {
                return BadRequest("Destination bank name is required.");
            }

            if (transferTime == null)
            {
                return BadRequest("Transfer time is required.");
            }

            var originBank = await repository.ReadOne(x => x.Name == origin);
            if(originBank == null)
            {
                return BadRequest("Unknown source bank.");
            }

            var destinationBank = await repository.ReadOne(x => x.Name == destination);
            if(destinationBank == null)
            {
                return BadRequest("Unknown destination bank.");
            }

            bool nextDay = false;
            var outcomingSessionTime = GetNextOutcomingSessionOfSameDay(originBank, transferTime);
            if(outcomingSessionTime == null)
            {
                outcomingSessionTime = GetFirstSessionOfTheDay(originBank, ElixirSessionType.Outcoming);
                nextDay = true;
            }

            if(outcomingSessionTime == null)
            {
                logger.LogError($"It looks like configuration of banks is invalid. Couldn't get outcoming session time for bank {origin}");
                return StatusCode(500);
            }

            var incomingSessionTime = GetNextIncomingSessionOfSameDay(destinationBank, (TimeSpan)outcomingSessionTime);
            if(incomingSessionTime == null)
            {
                incomingSessionTime = GetFirstSessionOfTheDay(destinationBank, ElixirSessionType.Incoming);
                nextDay = true;
            }

            if (incomingSessionTime == null)
            {
                logger.LogError($"It looks like configuration of banks is invalid. Couldn't get incoming session time for bank {destination}");
                return StatusCode(500);
            }

            return Ok(new PredictionResponse { Time = (TimeSpan)incomingSessionTime, NextDay = nextDay });
        }

        private TimeSpan? GetNextOutcomingSessionOfSameDay(Model.Bank bank, TimeSpan transferTime)
        {
            return bank
                .ElixirSessions
                .Where(x => x.Type == ElixirSessionType.Outcoming)
                .OrderBy(x => x.SessionTime)
                .FirstOrDefault(x => x.SessionTime > transferTime)?.SessionTime;
        }

        private TimeSpan? GetNextIncomingSessionOfSameDay(Model.Bank bank, TimeSpan outcomingSessionTime)
        {
            return bank
                .ElixirSessions
                .Where(x => x.Type == ElixirSessionType.Incoming)
                .OrderBy(x => x.SessionTime)
                .FirstOrDefault(x => x.SessionTime > outcomingSessionTime
                )?.SessionTime;
        }

        private TimeSpan? GetFirstSessionOfTheDay(Model.Bank bank, ElixirSessionType sessionType)
        {
            return bank.ElixirSessions.Where(x => x.Type == sessionType).OrderBy(x => x.SessionTime).First()?.SessionTime;
        }
    }
}