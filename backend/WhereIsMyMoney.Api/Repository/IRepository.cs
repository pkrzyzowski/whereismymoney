﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Repository
{
    public interface IRepository<T>
    {
        Task<T> Create(T item);
        Task<IEnumerable<T>> Read(Expression<Func<T, bool>> predicate = null);
        Task<T> ReadOne(Expression<Func<T, bool>> predicate);
        Task<T> Update(Expression<Func<T, bool>> predicate, T updatedItem);
        Task Delete(Expression<Func<T, bool>> predicate);
        Task<bool> Exists(Expression<Func<T, bool>> predicate);
    }
}
