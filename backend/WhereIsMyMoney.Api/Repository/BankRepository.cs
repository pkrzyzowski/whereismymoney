﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WhereIsMyMoney.Api.Model;

namespace WhereIsMyMoney.Api.Repository
{
    public class BankRepository : IRepository<Bank>
    {
        private IMongoCollection<Bank> collection;

        public BankRepository(IMongoCollection<Bank> collection)
        {
            this.collection = collection;
        }

        public async Task<Bank> Create(Bank item)
        {
            var existingItem = collection.AsQueryable().SingleOrDefault(x => x.Name == item.Name);
            if(existingItem != null)
            {
                throw new RepositoryException("Bank with the same name already exists.");
            }

            item.Created = DateTime.UtcNow;
            try
            {
                await collection.InsertOneAsync(item);
                return item;
            }
            catch (Exception e)
            {
                throw new RepositoryException("Couldn't create a bank in the database.", e);
            }
        }

        public async Task Delete(Expression<Func<Bank, bool>> predicate)
        {
            var result = await collection.DeleteOneAsync(Builders<Bank>.Filter.Where(predicate));
            if(result.DeletedCount == 1)
            {
                throw new RepositoryException($"Couldn't delete a bank from the repository.");
            }
        }

        public async Task<bool> Exists(Expression<Func<Bank, bool>> predicate)
        {
            var cursor = await collection.FindAsync(predicate);
            return cursor.Any();
        }

        public async Task<IEnumerable<Bank>> Read(Expression<Func<Bank, bool>> predicate = null)
        {
            var filter = predicate != null
                ? Builders<Bank>.Filter.Where(predicate)
                : Builders<Bank>.Filter.Empty;

            try
            {
                var banks = await collection.FindAsync(filter);
                return banks.ToEnumerable();
            }
            catch (Exception e)
            {
                throw new RepositoryException("Couldn't read bank objects from repository.", e);
            }
        }

        public async Task<Bank> ReadOne(Expression<Func<Bank, bool>> predicate)
        {
            var result = await Read(predicate);
            return result.FirstOrDefault();
        }

        public async Task<Bank> Update(Expression<Func<Bank, bool>> predicate, Bank replacement)
        {
            var updateDefinition = Builders<Bank>.Update
                .Set(x => x.Name, replacement.Name)
                .Set(x => x.ElixirSessions, replacement.ElixirSessions)
                .Set(x => x.LastModified, DateTime.UtcNow);

            try
            {
                var updatedBank = await collection.FindOneAndUpdateAsync(predicate, updateDefinition);
                return updatedBank;
            }
            catch (Exception e)
            {
                throw new RepositoryException("Couldn't update bank in the database.", e);
            }
        }
    }
}
    