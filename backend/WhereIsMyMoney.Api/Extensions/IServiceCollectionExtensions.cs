﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WhereIsMyMoney.Api.Model;
using WhereIsMyMoney.Api.Settings;

namespace WhereIsMyMoney.Api
{
    public static class IServiceCollectionExtensions
    {
        private const string BankCollectionName = "Bank";

        public static IServiceCollection SetupMongoDb(this IServiceCollection services, DatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            IMongoCollection<Bank> bankCollection;

            if(!database.ListCollectionNames().ToList().Contains(BankCollectionName))
            {
                bankCollection = database.GetCollection<Bank>(BankCollectionName);
                bankCollection.Indexes.CreateMany(new []
                {
                    new CreateIndexModel<Bank>(Builders<Bank>.IndexKeys.Ascending(field => field.Name))
                });
                bankCollection.InsertMany(InitialData.Banks);
            }
            else
            {
                bankCollection = database.GetCollection<Bank>(BankCollectionName);
            }

            services.AddSingleton<IMongoCollection<Bank>>(bankCollection);
            return services;
        }

        public static IServiceCollection ConfigureMapper(this IServiceCollection services)
        {
            IMapper mapper = new MapperConfiguration(config => {
                config.CreateMap<Dto.ElixirSession, Model.ElixirSession>()
                    .ForMember(x => x.SessionTime, x => x.ResolveUsing(v => TimeSpan.ParseExact(v.SessionTime, @"hh\:mm", CultureInfo.InvariantCulture)));

                config.CreateMap<Model.ElixirSession, Dto.ElixirSession>()
                    .ForMember(x => x.SessionTime, x => x.ResolveUsing(v => v.SessionTime.ToString(@"hh\:mm")));

                config.CreateMap<Dto.Bank, Model.Bank>()
                    .ForMember(x => x.Created, x => x.Ignore())
                    .ForMember(x => x.LastModified, x => x.Ignore());

                config.CreateMap<Model.Bank, Dto.Bank>();

            }).CreateMapper();

            services.AddScoped<IMapper>(x => mapper);

            return services;
        }
    }
}
