﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Dto
{
    public class Bank
    {
        public string Name { get; set; }
        public IList<ElixirSession> ElixirSessions { get; set; }
    }
}
