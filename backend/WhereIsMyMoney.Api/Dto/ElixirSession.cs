﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Dto
{
    public class ElixirSession
    {
        public Model.ElixirSessionType Type { get; set; }
        public string SessionTime { get; set; }
    }
}
