﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WhereIsMyMoney.Api.Dto
{
    public class PredictionResponse
    {
        public TimeSpan Time { get; set; }
        public bool NextDay { get; set; }
    }
}
